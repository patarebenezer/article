import DetailPage from "../DetailPage";
import ArticlePageList from "./ArticlePageList";

function TabContent1() {
 return <ArticlePageList />;
}

function TabContent2() {
 return <DetailPage />;
}

export { TabContent1, TabContent2 };
