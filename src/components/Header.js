import { useState } from "react";
import iconUk from "../components/assets/icons/icon-uk.png";
import iconUser from "../components/assets/icons/icon-user.png";
import iconNotif from "../components/assets/icons/notif.svg";

const RotateIconArrow = ({ initialValue, icon }) => {
 const [isRotated, setIsRotated] = useState(initialValue);
 const handleIconClick = () => {
  setIsRotated(!isRotated);
 };

 const iconSrc = icon === "flag" ? iconUk : iconUser;

 return (
  <li
   className='flex items-center gap-1 cursor-pointer'
   onClick={handleIconClick}
  >
   <img
    src={iconSrc}
    alt='gambar icon'
    className={`${icon === "flag" ? "" : "w-10"}`}
   />
   <svg
    className={`rotate-0 transition-transform ${isRotated ? "rotate-180" : ""}`}
    xmlns='http://www.w3.org/2000/svg'
    width='24'
    height='24'
    viewBox='0 0 24 24'
    fill='none'
   >
    <path
     d='M12 15.4L6 9.4L7.4 8L12 12.6L16.6 8L18 9.4L12 15.4Z'
     fill='#171717'
    />
   </svg>
  </li>
 );
};

const Header = () => {
 return (
  <div className='max-w-full h-20 flex justify-between items-center px-10'>
   <h1 className='text-2xl font-bold antialiased'>Article</h1>
   <div>
    <ul className='flex items-center list-none gap-10'>
     <RotateIconArrow initialValue={false} icon={"flag"} />
     <li className='border-r-2 pr-4'>
      <img src={iconNotif} alt='' />
     </li>
     <RotateIconArrow initialValue={false} icon={"user"} />
    </ul>
   </div>
  </div>
 );
};

export default Header;
