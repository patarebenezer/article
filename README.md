# Technical Interview Frontend Developer

## PT Seluruh Indonesia Online

Selamat datang di proyek Technical Interview Frontend Developer untuk PT Seluruh Indonesia Online. Proyek ini bertujuan untuk membuat halaman CRUD data artikel yang memenuhi sejumlah persyaratan teknis. Berikut adalah detail pengerjaan proyek ini:

### Persyaratan Utama

1. **UI Design**: Menerapkan slicing dari design UI berikut: [Figma UI Design](https://www.figma.com/file/QUJSUvqgMyR3Xsvfe2UyOY/Tes-Article)
2. **Javascript Framework**: Menggunakan Javascript framework yaitu React.
3. **Styling**: Styling menggunakan CSS framework atau vanilla.
4. **REST API**: Menerapkan pemrosesan dengan REST API dari [https://api-trials.x5.com.au/api/](https://api-trials.x5.com.au/api/).

### Dokumentasi API

Dokumentasi lengkap API dapat ditemukan di sini: [Dokumentasi API](https://www.postman.com/orange-crescent-369864/workspace/test-concepto/documentation/20369819-3cca02f3-92e6-49db-b039-4d9974e3007d).

### Fitur Tambahan

5. **Pagination dan Search**: List data menerapkan konsep pagination dan search.
6. **Version Control**: Project disimpan menggunakan Git (GitHub, Gitlab, dll).
7. **State Management (Optional)**: Menerapkan State Management seperti Redux, Context, dll (opsional).
8. **Deployment (Optional)**: Proyek dideploy secara online (Dapat kami akses) (opsional).

## Panduan Instalasi dan Penggunaan

1. Clone repositori ini ke komputer Anda.
2. Install semua dependensi dengan menjalankan perintah `npm install` atau `yarn install`.
3. Jalankan proyek dengan perintah `npm start` atau `yarn start`.
4. Buka browser Anda dan akses [http://localhost:3000](http://localhost:3000) untuk melihat proyek dalam mode pengembangan.

## Kontribusi

Jika Anda ingin berkontribusi pada proyek ini, silakan buat _fork_ dari repositori ini, lakukan perubahan yang diperlukan, dan ajukan _pull request_.

## Lisensi

Tidak ada lisensi yang diberikan untuk proyek ini.

Terima kasih telah berpartisipasi dalam Technical Interview Frontend Developer ini. Semoga sukses dalam pengerjaan proyek!
