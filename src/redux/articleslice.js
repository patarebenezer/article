import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
 data: [],
 status: "idle",
 error: null,
 page: 1,
 page_size: 5,
};

export const searchArticle = createAsyncThunk(
 "article/searchArticle",
 async ({ page, pageSize, search }) => {
  const response = await axios.get(
   `${process.env.REACT_APP_BASE_URL}articles?search=${search}&page=${page}&page_size=${pageSize}`
  );
  return response.data.data;
 }
);

export const fetchArticle = createAsyncThunk(
 "article/fetchArticle",
 async ({ page, pageSize }) => {
  const response = await axios.get(
   `${process.env.REACT_APP_BASE_URL}articles?page=${page}&page_size=${pageSize}`
  );
  return response.data.data;
 }
);

export const articleSlice = createSlice({
 name: "article",
 initialState,
 reducers: {
  setPageNumber: (state, action) => {
   state.page = action.payload;
  },
  setPageSizeNumber: (state, action) => {
   state.page_size = action.payload;
  },
 },
 extraReducers: (builder) => {
  builder
   .addCase(fetchArticle.pending, (state) => {
    state.status = "loading";
   })
   .addCase(fetchArticle.fulfilled, (state, action) => {
    state.status = "succeeded";
    state.data = action.payload;
   })
   .addCase(fetchArticle.rejected, (state, action) => {
    state.status = "failed";
    state.error = action.error.message;
   })
   .addCase(searchArticle.pending, (state) => {
    state.status = "loading";
   })
   .addCase(searchArticle.fulfilled, (state, action) => {
    state.status = "succeeded";
    state.data = action.payload;
   })
   .addCase(searchArticle.rejected, (state, action) => {
    state.status = "failed";
    state.error = action.error.message;
   });
 },
});

export default articleSlice.reducer;
