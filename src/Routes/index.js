import { createBrowserRouter } from "react-router-dom";
import DashboardPage from "../pages/DashboardPage";
import ErrorPage from "../pages/ErrorPage";
import DetailPage from "../pages/DetailPage";

const router = createBrowserRouter([
 {
  path: "/",
  element: <DashboardPage />,
 },
 {
  path: "/articles/add",
  element: <DetailPage />,
 },
 {
  path: "/*",
  element: <ErrorPage />,
 },
]);

export default router;
