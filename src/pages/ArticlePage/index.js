import { useState } from "react";
import ArticlePageList from "./ArticlePageList";
import DetailPage from "../DetailPage";
import TabMenu from "../../components/TabMenu";
import SearchBar from "../../components/SearchBar";

const ArticlePage = () => {
 const [activeTab, setActiveTab] = useState("tab1");
 const renderTabContent = () => {
  switch (activeTab) {
   case "tab1":
    return (
     <ArticlePageList activeTab={activeTab} setActiveTab={setActiveTab} />
    );
   case "tab2":
    return <DetailPage />;
   default:
    return null;
  }
 };
 return (
  <div className='bg-gray-200 h-screen p-5'>
   <TabMenu activeTab={activeTab} setActiveTab={setActiveTab} />
   <SearchBar activeTab={activeTab} setActiveTab={setActiveTab} />
   {renderTabContent()}
  </div>
 );
};

export default ArticlePage;
