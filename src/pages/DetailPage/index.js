const DetailPage = () => {
 return (
  <>
   <div className='bg-white grid gap-7 p-5 mt-5'>
    <header className='w-full border-b text-lg font-bold antialiased'>
     Add / Edit
    </header>
    <div className='grid'>
     <label className='text-gray-500 mb-1'>Title</label>
     <input
      type='text'
      className='border h-8 px-3 bg-slate-100 rounded-md w-1/2'
     />
    </div>
    <div className='grid'>
     <label className='text-gray-500 mb-1'>Content</label>
     <textarea
      type='text'
      className='border bg-slate-100 rounded-md w-9/12 h-24 px-3 py-1'
     />
    </div>
    <button className='bg-green-500 hover:bg-green-600 text-white rounded-lg py-2 px-5 w-32'>
     Save
    </button>
   </div>
  </>
 );
};

export default DetailPage;
