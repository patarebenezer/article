import ArticlePage from "../ArticlePage";
import Header from "../../components/Header";
import Sidebar from "../../components/Sidebar";

const DashboardPage = () => {
 return (
  <div>
   <div className='flex'>
    <Sidebar />
    <div className='w-full'>
     <Header />
     <ArticlePage />
    </div>
   </div>
  </div>
 );
};

export default DashboardPage;
