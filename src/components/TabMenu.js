import React from "react";
import list from "../components/assets/icons/group.png";
import add from "../components/assets/icons/add.png";

const TabMenu = ({ activeTab, setActiveTab }) => {
 const handleTabClick = (tab) => {
  setActiveTab(tab);
 };

 return (
  <div className='flex gap-40 border-b border-b-gray-300'>
   <div
    className={`flex gap-3 items-center cursor-pointer w-1/4 ${
     activeTab === "tab1" ? "border-b-2 border-b-green-500" : "text-gray-600"
    }`}
    onClick={() => handleTabClick("tab1")}
   >
    <div
     className={`border p-4 rounded-full ${
      activeTab === "tab1" ? "border-green-400" : "border-gray-400 "
     }`}
    >
     <img src={list} alt='Group icon' className='w-5 h-5' />
    </div>
    <div
     className={`font-bold ${activeTab === "tab1" ? "text-green-600" : ""}`}
    >
     <p className='antialiased'>Article</p>
     <span className='text-sm'>List Article</span>
    </div>
   </div>
   <div
    className={`flex gap-3 items-center cursor-pointer w-1/4 ${
     activeTab === "tab2" ? "border-b-2 border-b-green-500" : "text-gray-600"
    }`}
    onClick={() => handleTabClick("tab2")}
   >
    <div
     className={`border p-4 rounded-full ${
      activeTab === "tab2" ? "border-green-400" : "border-gray-400 "
     }`}
    >
     <img src={add} alt='Group icon' className='w-5 h-5' />
    </div>
    <p className={`font-bold ${activeTab === "tab2" ? "text-green-600" : ""}`}>
     <p>Add / Edit</p>
     <span className='text-sm'>Detail Article</span>
    </p>
   </div>
  </div>
 );
};

export default TabMenu;
