import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchArticle } from "../../redux/articleslice";
import Table from "../../components/Table";

const ArticlePageList = ({ activeTab, setActiveTab }) => {
 const dispatch = useDispatch();
 const articles = useSelector((state) => state.article.data);
 const status = useSelector((state) => state.article.status);
 const error = useSelector((state) => state.article.error);
 const [pageSize, setPageSize] = useState(5);
 const [totalPages, setTotalPages] = useState(1);

 useEffect(() => {
  if (status === "idle") {
   dispatch(fetchArticle({ page: 1, pageSize }));
  }
 }, [status, dispatch, pageSize]);

 useEffect(() => {
  if (articles?.page_info?.total) {
   setTotalPages(Math.ceil(articles.page_info.total / pageSize));
  }
 }, [articles, pageSize]);

 if (status === "error") {
  return <div>Error : {error}</div>;
 }

 return (
  <>
   <div
    className={`bg-white my-2 w-full h-4/5 ${
     pageSize > 5 && "overflow-scroll"
    }`}
   >
    <Table
     data={articles}
     setPageSize={setPageSize}
     pageSize={pageSize}
     totalPages={totalPages}
     setActiveTab={setActiveTab}
     activeTab={activeTab}
    />
   </div>
  </>
 );
};

export default ArticlePageList;
