import { Link } from "react-router-dom";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchArticle } from "../redux/articleslice";
import formatDate from "./utils/formatDate";
import edit from "../components/assets/icons/edit.png";
import deleteIcon from "../components/assets/icons/delete.png";
import ModalDelete from "./ModalDelete";

const Table = ({ data, setPageSize, pageSize, totalPages, setActiveTab }) => {
 const dispatch = useDispatch();
 const status = useSelector((state) => state.article.status);
 const [deleteItem, setDeleteItem] = useState(null);

 const handleOption = (e) => {
  setPageSize(parseInt(e.target.value));
  dispatch(fetchArticle({ page: 1, pageSize: parseInt(e.target.value) }));
 };

 const handleEdit = (id) => {
  setActiveTab("tab2");
 };

 const handleDelete = (item) => {
  setDeleteItem(item);
 };

 const cancelDelete = () => {
  setDeleteItem(null);
 };

 const confirmDelete = () => {
  setDeleteItem(null);
 };
 return (
  <>
   <ModalDelete
    confirmDelete={confirmDelete}
    cancelDelete={cancelDelete}
    deleteItem={deleteItem}
   />
   <table className='w-full h-3/5 mt-4'>
    <thead className='bg-[#e8f2e8] text-green-600 h-10 text-sm'>
     <th className='border border-green-300 w-1/4 font-normal'>Date</th>
     <th className='border border-green-300 w-1/4 font-normal'>Title</th>
     <th className='border border-green-300 w-1/3 font-normal'>Content</th>
     <th className='border border-green-300 w-1/4 font-normal'>Action</th>
    </thead>
    <tbody>
     {status === "loading" ? (
      <tr className='text-center text-slate-500 w-full border'>
       <td colSpan='4'>Loading...</td>
      </tr>
     ) : (
      data?.articles?.map((item) => (
       <tr className='text-center text-gray-600 border' key={item.id}>
        <td className='border p-2'>{formatDate(item?.created_at)}</td>
        <td className='border overflow-hidden whitespace-nowrap text-overflow-ellipsis max-w-[200px]'>
         {item?.title}
        </td>
        <td className='border overflow-hidden whitespace-nowrap text-overflow-ellipsis max-w-[300px]'>
         <p className='px-4'>{item?.content}</p>
        </td>
        <td className='flex justify-center items-center gap-2 h-full'>
         <div
          className='bg-[#CF8812] rounded-full p-2 cursor-pointer'
          onClick={() => handleEdit(item.id)}
         >
          <img src={edit} alt='edit icon' />
         </div>

         <div
          className='bg-[#FF1D1D] rounded-full p-2 cursor-pointer'
          onClick={() => handleDelete(item.id)}
         >
          <img src={deleteIcon} alt='delete icon' />
         </div>
        </td>
       </tr>
      ))
     )}
    </tbody>
   </table>

   <tfoot className='py-3 flex justify-end items-center mt-7'>
    <div className='mr-8 flex items-center gap-4'>
     <p className='text-sm'>Show</p>
     <select
      name='page_size'
      className='bg-gray-100 py-1 px-3'
      onChange={handleOption}
      value={pageSize}
     >
      <option value='5'>5</option>
      <option value='10'>10</option>
      <option value='20'>20</option>
      <option value='30'>30</option>
     </select>
     <p className='text-sm'>Entries</p>
    </div>
    <div className='flex space-x-2 mr-4'>
     {Array.from({ length: totalPages }, (_, index) => (
      <Link
       to={`#page-${index + 1}`}
       key={index}
       className={`px-3 py-1 text-green-500 rounded-md ${
        index + 1 === data?.page_info?.current_page
         ? "bg-green-600 text-white"
         : "bg-gray-100"
       }`}
       onClick={() => dispatch(fetchArticle({ page: index + 1, pageSize }))}
      >
       {index + 1}
      </Link>
     ))}
    </div>
   </tfoot>
  </>
 );
};

export default Table;
