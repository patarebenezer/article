import { configureStore } from "@reduxjs/toolkit";
import articleReducer from "./articleslice";

export const store = configureStore({
 reducer: {
  article: articleReducer,
 },
});

export default store;
