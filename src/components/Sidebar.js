import { useState } from "react";
import logo from "../components/assets/icons/icon-logo.png";
import arrow from "../components/assets/icons/sidebar.png";
import iconArticle from "../components/assets/icons/icon-article.png";

const Sidebar = () => {
 const [minimize, setMinimize] = useState(false);
 const hideText = !minimize && <p className='text-sm'>Article</p>;
 const hideTextLogo = !minimize && <p>Logo</p>;

 return (
  <div className={`${minimize ? "w-[5rem]" : "w-1/5"} h-screen border-r`}>
   <div className={`p-4 ${minimize && "flex justify-center"}`}>
    <img
     src={arrow}
     alt='Arrow'
     className='w-7 h-7 cursor-pointer'
     onClick={() => setMinimize(!minimize)}
    />
   </div>
   <div className='my-3 flex gap-1 justify-center items-center'>
    <img src={logo} alt='Logo Article' className=' w-10' />
    <p className='text-sm font-bold uppercase antialiased text-green-700'>
     {hideTextLogo}
    </p>
   </div>
   <nav
    className={`bg-green-50 text-green-500 p-3 font-bold my-10 border-l-4 border-l-green-500 flex items-center gap-2 cursor-pointer hover:bg-green-100 ${
     minimize ? "w-full justify-center" : "m-4"
    }`}
   >
    <img src={iconArticle} alt='Icon Article' className='w-7 h-5' />
    {hideText}
   </nav>
  </div>
 );
};

export default Sidebar;
