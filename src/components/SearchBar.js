import { useDispatch, useSelector } from "react-redux";
import { searchArticle } from "../redux/articleslice";
import searchIcon from "../components/assets/icons/search.svg";

const SearchBar = ({ setActiveTab }) => {
 const page = useSelector((state) => state.article.page);
 const pageSize = useSelector((state) => state.article.page_size);
 const dispatch = useDispatch();
 const handleSearch = (searchQuery) => {
  dispatch(searchArticle({ page, pageSize, search: searchQuery }));
 };

 return (
  <div className='py-5 px-3 flex justify-between bg-white mb-[-1rem] mt-3'>
   <div className='flex relative w-1/3'>
    <input
     type='text'
     placeholder='Type here to search'
     className='w-full rounded-lg pl-10 py-1 text-sm border'
     onChange={(e) => handleSearch(e.target.value)}
    />
    <img
     src={searchIcon}
     alt='Search Icon'
     className='absolute left-3 top-1/2 transform -translate-y-1/2'
    />
   </div>
   <div className='flex gap-5'>
    <select name='tahun' className='bg-slate-200 px-2 py-1 rounded'>
     <option value='2022'>2022</option>
     <option value='2023' selected>
      2023
     </option>
    </select>
    <button
     className='bg-green-500 text-white font-semibold antialiased py-1 px-4 rounded-lg'
     onClick={() => setActiveTab("tab2")}
    >
     + Add
    </button>
   </div>
  </div>
 );
};

export default SearchBar;
